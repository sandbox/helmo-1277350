<?php

/*
 * @file
 *   Tests for archive-dump
 */
class archiveDumpCase extends Drush_CommandTestCase {

  /*
   * Test dump and extraction.
   */
  public function testArchiveDump() {
    $sites = $this->setUpDrupal(1, TRUE);
    $site = reset($sites);
    $root = $this->webroot();
    $uri = key($sites);
    $docroot = basename($root);

    $dump_dest = "dump.tar.gz";
    $options = array(
      'root' => $root,
      'uri' => $uri,
      'yes' => NULL,
      'destination' => $dump_dest,
    );
    $this->drush('archive-dump', array($uri), $options);
    $exec = sprintf('file %s/%s', UNISH_SANDBOX, $dump_dest);
    $this->execute($exec);
    $output = $this->getOutput();
    $expected = UNISH_SANDBOX . "/dump.tar.gz: gzip compressed data, from Unix";
    $this->assertEquals($expected, $output);

    // Untar it, make sure it looks right.
    $untar_dest = UNISH_SANDBOX . '/untar';
    $exec = sprintf('mkdir %s && cd %s && tar xzf %s/%s', $untar_dest, $untar_dest, UNISH_SANDBOX, $dump_dest);
    $this->execute($exec);

    if (strpos(UNISH_DB_URL, 'mysql') !== FALSE) {
      $this->execute(sprintf('head %s/unish_%s.sql | grep "MySQL dump"', $untar_dest, $uri));
    }
    $this->assertFileExists($untar_dest . '/MANIFEST.ini', 'MANIFEST.ini should exist');
    $this->execute('test -d ' . $untar_dest . '/' . $docroot);

    // Cleanup
    unish_file_delete_recursive($untar_dest);
    unish_file_delete_recursive(UNISH_SANDBOX . '/' . $dump_dest);
  }

  /*
   * Test dump and extraction with --no-core.
   */
  public function testArchiveDumpNoCore() {
    $sites = $this->setUpDrupal(1, TRUE);
    $site = reset($sites);
    $root = $this->webroot();
    $uri = key($sites);
    $docroot = basename($root);

    $dump_dest = "dump.tar.gz";
    $options = array(
      'root' => $root,
      'uri' => $uri,
      'yes' => NULL,
      'no-core' => TRUE,
      'destination' => $dump_dest,
    );
    $this->drush('archive-dump', array($uri), $options);
    $exec = sprintf('file %s/%s', UNISH_SANDBOX, $dump_dest);
    $this->execute($exec);
    $output = $this->getOutput();
    $expected = UNISH_SANDBOX . "/dump.tar.gz: gzip compressed data, from Unix";
    $this->assertEquals($expected, $output);

    // Untar it, make sure it looks right.
    $untar_dest = UNISH_SANDBOX . '/untar';
    $exec = sprintf('mkdir %s && cd %s && tar xzf %s/%s', $untar_dest, $untar_dest, UNISH_SANDBOX, $dump_dest);
    $this->execute($exec);

    if (strpos(UNISH_DB_URL, 'mysql') !== FALSE) {
      $this->execute(sprintf('head %s/unish_%s.sql | grep "MySQL dump"', $untar_dest, $uri));
    }
    $this->assertFileExists($untar_dest . '/MANIFEST.ini', 'MANIFEST.ini should exist');
    $this->execute('test -d ' . $untar_dest . '/' . $docroot);
    $this->assertFileNotExists($untar_dest . '/' . $docroot . '/modules', 'No modules directory should exist with --no-core');

    // Cleanup
    unish_file_delete_recursive($untar_dest);
    unish_file_delete_recursive(UNISH_SANDBOX . '/' . $dump_dest);
  }


  /*
   * Test dump and extraction with --no-core.
   */
  public function testArchiveRestoreNoCore() {
    $sites = $this->setUpDrupal(1, TRUE);
    $site = reset($sites);
    $root = $this->webroot();
    $uri = key($sites);
    $docroot = basename($root);

    $dump_dest = "dump.tar.gz";
    $dump_dest_path = UNISH_SANDBOX . '/' . $dump_dest ;
    $options = array(
      'root' => $root,
      'uri' => $uri,
      'yes' => NULL,
      'no-core' => TRUE,  // <<-- The option being tested here
      'destination' => $dump_dest,
    );
    $this->drush('archive-dump', array($uri), $options);

    unish_file_delete_recursive($root . '/sites/dev');
    $this->assertFileNotExists($root . '/sites/dev/settings.php', 'settings.php should have been removed before the restore');
    $options = array(
        'overwrite' => TRUE,
        );
   
    // Do the restore
    $this->drush('archive-restore',array($dump_dest_path), $options);

    // Verify
    $this->assertFileExists($root . '/index.php', 'index.php should still exist');
    $this->assertFileExists($root . '/sites/dev/settings.php', 'settings.php should have been recreated by the restore');

    // Cleanup
    unish_file_delete_recursive(UNISH_SANDBOX . '/' . $dump_dest);
  }
}
